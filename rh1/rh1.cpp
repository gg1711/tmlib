/**
 * Code for HyTM is loosely based on the code for TL2
 * (in particular, the data structures)
 *
 * This is an implementation of rh1 algorithm from reduced hardware paper.
 *
 * [ note: we cannot distribute this without inserting the appropriate
 *         copyright notices as required by TL2 and STAMP ]
 *
 * Authors: Trevor Brown (trevor.brown@uwaterloo.ca) and Srivatsan Ravi
 */

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include "rh1.h"
#include "../hytm1/platform_impl.h"
#include "stm.h"
// #include "tmalloc.h"
#include "util.h"
#include <iostream>
#include <execinfo.h>
#include <stdint.h>
using namespace std;

#define _XABORT_EXPLICIT_LOCKED 1

#define USE_FULL_HASHTABLE
//#define USE_BLOOM_FILTER

#define HASHTABLE_CLEAR_FROM_LIST

#define MALLOC_PADDED(sz) ((void *) (((char *) malloc((sz) + 2*PREFETCH_SIZE_BYTES)) + PREFETCH_SIZE_BYTES))
#define FREE_PADDED(x) free((void *) (((char *) (x)) - PREFETCH_SIZE_BYTES))


// just for debugging
PAD;
volatile int globallock = 0;
PAD;

PAD;
volatile int global_clock = 2;
PAD;

#define GSIZE (1<<20)

PAD;
// holds timestamp / stripe_version of recently accessed value
static intptr_t stripe_version_array[GSIZE];
PAD;



void printStackTrace() {

  void *trace[16];
  char **messages = (char **)NULL;
  int i, trace_size = 0;

  trace_size = backtrace(trace, 16);
  messages = backtrace_symbols(trace, trace_size);
  /* skip first stack frame (points here) */
  printf("  [bt] Execution path:\n");
  for (i=1; i<trace_size; ++i)
  {
    printf("    [bt] #%d %s\n", i, messages[i]);

    /**
     * find first occurrence of '(' or ' ' in message[i] and assume
     * everything before that is the file name.
     */
    int p = 0; //size_t p = 0;
    while(messages[i][p] != '(' && messages[i][p] != ' '
            && messages[i][p] != 0)
        ++p;

    char syscom[256];
    sprintf(syscom,"echo \"    `addr2line %p -e %.*s`\"", trace[i], p, messages[i]);
        //last parameter is the file name of the symbol
    if (system(syscom) < 0) {
        printf("ERROR: could not run necessary command to build stack trace\n");
        exit(-1);
    };
  }

  exit(-1);
}

void initSighandler() {
    /* Install our signal handler */
    struct sigaction sa;

    sa.sa_handler = (sighandler_t) /*(void *)*/ printStackTrace;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;

    sigaction(SIGSEGV, &sa, NULL);
    sigaction(SIGUSR1, &sa, NULL);
}

/** used only for debugging **/
void __acquireLock(volatile int *lock) {
    while (1) {
        if (*lock) {
            PAUSE();
            continue;
        }
        LWSYNC; // prevent the following CAS from being moved before read of lock (on power)
        if (__sync_bool_compare_and_swap(lock, 0, 1)) {
            SYNC_RMW; // prevent instructions in the critical section from being moved before the lock (on power)
            return;
        }
    }
}

/** used only for debugging **/
void __releaseLock(volatile int *lock) {
    LWSYNC; // prevent unlock from being moved before instructions in the critical section (on power)
    *lock = 0;
}





#include <map>
PAD;
map<const void*, unsigned> addrToIx;
map<unsigned, const void*> ixToAddr;
volatile unsigned rename_ix = 0;
PAD;
#include <sstream>
string stringifyIndex(size_t ix) {
#if 1
    const unsigned NCHARS = 36;
    stringstream ss;
    if (ix == 0) return "0";
    while (ix > 0) {
        unsigned newchar = ix % NCHARS;
        if (newchar < 10) {
            ss<<(char)(newchar+'0');
        } else {
            ss<<(char)((newchar-10)+'A');
        }
        ix /= NCHARS;
    }
    string backwards = ss.str();
    stringstream ssr;
    for (string::reverse_iterator rit = backwards.rbegin(); rit != backwards.rend(); ++rit) {
        ssr<<*rit;
    }
    return ssr.str();
#elif 0
    const unsigned NCHARS = 26;
    stringstream ss;
    if (ix == 0) return "0";
    while (ix > 0) {
        unsigned newchar = ix % NCHARS;
        ss<<(char)(newchar+'A');
        ix /= NCHARS;
    }
    return ss.str();
#else
    stringstream ss;
    ss<<ix;
    return ss.str();
#endif
}
string renamePointer(const void* p) {
    DEBUG3 {
        map<const void*, unsigned>::iterator it = addrToIx.find(p);
        if (it == addrToIx.end()) {
            unsigned newix = __sync_fetch_and_add(&rename_ix, 1);
            addrToIx[p] = newix;
            ixToAddr[newix] = p;
            return stringifyIndex(addrToIx[p]);
        } else {
            return stringifyIndex(it->second);
        }
    } else {
        return stringifyIndex((size_t) (uintptr_t) p);
    }
}

/*
 * Consider 4M alignment for LockTab so we can use large-page support.
 * Alternately, we could mmap() the region with anonymous DZF pages.
 */
#define _TABSZ  (1<<20) /*(1<<3)*/
// #define STACK_SPACE_LOCKTAB
// #ifdef STACK_SPACE_LOCKTAB
// PAD;
// static vLock LockTab[_TABSZ];
// PAD;
// #else
// struct vLockSpace {
//     char buf[sizeof(vLock)];
// };
// PAD;
// static vLockSpace *LockTab;
// PAD;
// #endif

/*
 * With PS the versioned lock words (the LockTab array) are table stable and
 * references will never fault.  Under PO, however, fetches by a doomed
 * zombie txn can fault if the referent is free()ed and unmapped
 */
// #if 0
// #define LDLOCK(a)                     LDNF(a)  /* for PO */
// #else
// #define LDLOCK(a)                     *(a)     /* for PS */
// #endif

/*
 * PSLOCK: maps variable address to lock address.
 * For PW the mapping is simply (UNS(addr)+sizeof(int))
 * COLOR attempts to place the lock(metadata) and the data on
 * different D$ indexes.
 */
#define TABMSK (_TABSZ-1)
#define COLOR 0 /*(128)*/

/*
 * ILP32 vs LP64.  PSSHIFT == Log2(sizeof(intptr_t)).
 */
#define PSSHIFT ((sizeof(void*) == 4) ? 2 : 3)
// #define PSLOCK(a) ((vLock*) (LockTab + (((UNS(a)+COLOR) >> PSSHIFT) & TABMSK))) /* PS1M */
//#define PSLOCK(a) ({ uintptr_t ___p = (UNS((a)) >> PSSHIFT); ___p ^= ___p >> 16; ___p *= 0x85ebca6b; ___p ^= ___p >> 13; ___p *= 0xc2b2ae35; ___p ^= ___p >> 16; ((vLock*) (LockTab + (___p & TABMSK))); }) /* scatter addr->lock mapping with murmurhash3 */


/**
 * There are two ways to implement it - 
 * first - we can use something like PSLOCK(a) to map directly addresses to index     
 * if this is possible not implement, we can use unordered_map and counter for 
 * assigning index to address. but this can cause global contention as multiple threads try 
 * to read it at same time.
 */

#define get_stripe_index(a) (((UNS(a)+COLOR) >> PSSHIFT) & TABMSK)


/**
 *
 * THREAD CLASS
 *
 */

//class TypeLogs;
#include <vector>
#include <iterator>
#include <unordered_map>

class Thread {
public:
    PAD;
    long UniqID;
    volatile long Retries;
//    int* ROFlag; // not used by stamp
    int IsRO;
    int isFallback;
//    long Starts; // how many times the user called TxBegin
    long AbortsHW; // # of times hw txns aborted
    long AbortsSW; // # of times sw txns aborted
    long CommitsHW;
    long CommitsSW;
    unsigned long long rng;
    unsigned long long xorrng [1];
    // tmalloc_t* allocPtr;    /* CCM: speculatively allocated */
    // tmalloc_t* freePtr;     /* CCM: speculatively free'd */
//    TypeLogs* rdSet;
//    TypeLogs* wrSet;
//    TypeLogs* LocalUndo;
    intptr_t tx_version;
    intptr_t next_ver;
    // List* rdSet;
    // List* wrSet;
    vector<volatile intptr_t*> rdSet;
    vector< pair<volatile intptr_t*, intptr_t> > wrSet; //it should be pair<uint64_t*,value> value to be written at this addr
    unordered_map<volatile intptr_t*, intptr_t> uMap; // to check if addr present in wrSet and stores its index;
    sigjmp_buf* envPtr;
    PAD;

    Thread(long id);
    void destroy();
    void compileTimeAsserts() {
        CTASSERT(sizeof(*this) == sizeof(Thread_void));
    }
};// __attribute__((aligned(CACHE_LINE_SIZE)));




/**
 *
 * THREAD CLASS IMPLEMENTATION
 *
 */

PAD;
volatile long StartTally = 0;
volatile long AbortTallyHW = 0;
volatile long AbortTallySW = 0;
volatile long CommitTallyHW = 0;
volatile long CommitTallySW = 0;
PAD;

Thread::Thread(long id) {
    DEBUG1 aout("new thread with id "<<id);
    // memset(this, 0, sizeof(Thread)); /* Default value for most members */
    UniqID = id;
    rng = id + 1;
    xorrng[0] = rng;

    next_ver = global_clock;
    tx_version = global_clock;

    // wrSet = (List*) malloc(sizeof(*wrSet));//(TypeLogs*) malloc(sizeof(TypeLogs));
    // rdSet = (List*) malloc(sizeof(*rdSet));//(TypeLogs*) malloc(sizeof(TypeLogs));
    // //LocalUndo = (TypeLogs*) malloc(sizeof(TypeLogs));
    // wrSet->init(this, INIT_WRSET_NUM_ENTRY);
    // rdSet->init(this, INIT_RDSET_NUM_ENTRY);
    // //LocalUndo->init(this, INIT_LOCAL_NUM_ENTRY);

    // allocPtr = tmalloc_alloc(1);
    // freePtr = tmalloc_alloc(1);
    // assert(allocPtr);
    // assert(freePtr);
}

void Thread::destroy() {

}


/**
 *
 * IMPLEMENTATION OF TM OPERATIONS
 *
 */

void TxClearRWSets(void* _Self) {
    Thread* Self = (Thread*) _Self;

    Self->rdSet.clear();
    Self->wrSet.clear();
    Self->uMap.clear();
}



inline void validateUsing_htm(Thread* Self){

    //read-set revalidation
    for(auto &addr:Self->rdSet){
        int s_index = get_stripe_index(addr);
        intptr_t version = stripe_version_array[s_index];
        if( version > Self->tx_version){
            // version modified later so abort Tx
            XABORT(0);
            return;
        }
    }
    // perform actual writes
    intptr_t next_ver = global_clock;

    for(auto &itr:Self->wrSet){
        auto &addr = itr.first;
        auto &new_value = itr.second;
        int s_index = get_stripe_index(addr);
        stripe_version_array[s_index] = next_ver;
        // cout<<"old value "<<*addr;
        *addr = new_value;
        // cout<<" new_value "<<*addr<<endl;
    }
    return;
    //HTM_COMMIT()
    //failed cases is mentioned in STM.h so need to change it there. 
}

void TxCommit_stm(Thread* Self){
    // read-only Tx commit immediately
    if( Self->wrSet.empty()) 
        return;

    // validateUsing_htm(Self);

    int ___htm_attempts = 0;
    XBEGIN_ARG_T ___xarg;
    for (___htm_attempts = 0 ; ___htm_attempts < HTM_ATTEMPT_THRESH; ++___htm_attempts) {
        if (XBEGIN(___xarg)) {
            validateUsing_htm(Self);
            XEND();
            break;
        }
        /* else if(X_ABORT_STATUS_IS_CAPACITY(___xarg)){
            // currently we are aborting in fast RH1
            //else we can make it fallback to RH2
            TxAbort(_Self);
            break;
         }
        */
    }

    if (___htm_attempts == HTM_ATTEMPT_THRESH) {
        // aborting it
         TxAbort(Self);
    }    
}

int TxCommit(void* _Self) {
    Thread* Self = (Thread*) _Self;
    // software path
    if (Self->isFallback) {
        SOFTWARE_BARRIER; // prevent compiler reordering of speculative execution before isFallback check in htm (for power)
        TxCommit_stm(Self);
    } 
    // hardware path
    else { 
        XEND();
        ++Self->CommitsHW;
        TM_COUNTER_INC(htmCommit[PATH_FAST_HTM], Self->UniqID);
    }

success:
// #ifdef TXNL_MEM_RECLAMATION
//     // "commit" speculative frees and speculative allocations
//     tmalloc_releaseAllForward(Self->freePtr, NULL);
//     tmalloc_clear(Self->allocPtr);
// #endif
    return true;
}

void TxAbort(void* _Self) {
    Thread* Self = (Thread*) _Self;
    __sync_fetch_and_add(&global_clock, 2);
    // software path
    if (Self->isFallback) {
        SOFTWARE_BARRIER; // prevent compiler reordering of speculative execution before isFallback check in htm (for power)
        DEBUG2 aout("thread "<<Self->UniqID<<" software abort...");
        ++Self->Retries;
        ++Self->AbortsSW;
        if (Self->Retries > MAX_RETRIES) {
            aout("TOO MANY ABORTS. QUITTING.");
            aout("BEGIN DEBUG ADDRESS MAPPING:");
            __acquireLock(&globallock);
                for (unsigned i=0;i<rename_ix;++i) {
                    cout<<stringifyIndex(i)<<"="<<ixToAddr[i]<<" ";
                }
                cout<<endl;
            __releaseLock(&globallock);
            aout("END DEBUG ADDRESS MAPPING.");
            exit(-1);
        }
        TM_REGISTER_ABORT(PATH_FALLBACK, 0, Self->UniqID);

// #ifdef TXNL_MEM_RECLAMATION
//         // "abort" speculative allocations and speculative frees
//         tmalloc_releaseAllReverse(Self->allocPtr, NULL);
//         tmalloc_clear(Self->freePtr);
// #endif

        // longjmp to start of txn
        LWSYNC; // prevent any writes after the longjmp from being moved before this point (on power) // TODO: is this needed?
        SIGLONGJMP(*Self->envPtr, 1);
        ASSERT(0);

    // hardware path
    } else {
        XABORT(0);
    }
}

intptr_t TxLoad_stm(void* _Self, volatile intptr_t* addr) {
    Thread* Self = (Thread*) _Self;

    // check whether addr is in the write-set
    auto itr = Self->uMap.find(addr);
    if(itr != Self->uMap.end()) return Self->wrSet[itr->second].second; //uMap[addr] = idx of wrSet<addr,value> where value is stored;

    // log the read
    Self->rdSet.push_back(addr);

    //try to read the memory location
    int s_index = get_stripe_index((addr));
    intptr_t ver_before = stripe_version_array[s_index];
    intptr_t val = *addr;
    intptr_t ver_after = stripe_version_array[s_index];

    // if version changed ...Changed pseudocode to avoid control overflow error 
    if(ver_before > Self->tx_version || ver_before!=ver_after)
        TxAbort(_Self); 

    return val;
}

intptr_t TxLoad_htm(void* _Self, volatile intptr_t* addr) {
    return *addr;
}

void TxStore_stm(void* _Self, volatile intptr_t* addr, intptr_t value) {
    Thread* Self = (Thread*) _Self;

    // check whether addr is in the write-set ?? Do we need to check it?
    // Not given in pseudo code 
    // else use map to search its presence

    Self->wrSet.push_back({addr,value});
    Self->uMap[addr] = (Self->wrSet.size() - 1);

    // int s_index = get_stripe_index(addr);
    // assert(s_index < GSIZE);

    // int cnt = 0;
    // int ___htm_attempts = 0;
    // XBEGIN_ARG_T ___xarg;
    // for (___htm_attempts = 0 ; ___htm_attempts < HTM_ATTEMPT_THRESH; ++___htm_attempts) {
    //     if (XBEGIN(___xarg)) {
    //         volatile intptr_t next_ver = global_clock;

    //         for(auto &itr:Self->wrSet){
    //             auto _addr = itr.first;
    //             auto new_value = itr.second;
    //             int s_index = get_stripe_index(_addr);
    //             stripe_version_array[s_index] = next_ver;
    //             // cout<<"old value "<<*addr;
    //             *_addr = new_value;
    //             // cout<<" new_value "<<*addr<<endl;
    //         }
            
    //         ++cnt;
    //         XEND();
    //         break;
    //     }
    //     else{
    //         // cout<<"failed to load htm"<<endl;
    //     }
    // }
    // cout<<"value of cnt "<<cnt<<endl;
}

void TxStore_htm(void* _Self, volatile intptr_t* addr, intptr_t value) {
    Thread* Self = (Thread*) _Self;
    //update write location version
    int s_index = get_stripe_index(addr);
    stripe_version_array[s_index] = Self->next_ver;
    //write value to memory
    *addr = value;
}










/**
 *
 * FRAMEWORK FUNCTIONS
 * (PROBABLY DON'T NEED TO BE CHANGED WHEN CREATING A VARIATION OF THIS TM)
 *
 */

void TxOnce() {
    TM_CREATE_COUNTERS();
    printf("%s %s\n", TM_NAME, "system ready\n");
}

void TxClearCounters() {
    printf("Printing counters for %s and then clearing them in preparation for the real trial.\n", TM_NAME);
    TM_PRINT_COUNTERS();
    TM_CLEAR_COUNTERS();
    printf("Counters cleared.\n");
}

void TxShutdown() {
    printf("%s system shutdown:\n    HTM_ATTEMPT_THRESH=%d\n"
                , TM_NAME
                , HTM_ATTEMPT_THRESH
    );
    if (!__tm_counters) return; // didn't actually invoke TxOnce, so destructor not needed

    TM_PRINT_COUNTERS();
    TM_DESTROY_COUNTERS();
}

void* TxNewThread() {
    Thread* t = (Thread*) malloc(sizeof(Thread));
    assert(t);
    return t;
}

void TxFreeThread(void* _t) {
    Thread* t = (Thread*) _t;
    t->destroy();
    free(t);
}

void TxInitThread(void* _t, long id) {
    Thread* t = new(_t) Thread(id);
    // *t = Thread(id);c
}

// void TxInitThread(void* _t, long id) {
//     Thread* t = (Thread*) _t;
//     *t = Thread(id);
// }


/* =============================================================================
 * TxAlloc
 *
 * CCM: simple transactional memory allocation
 * =============================================================================
 */
void* TxAlloc(void* _Self, size_t size) {
// #ifdef TXNL_MEM_RECLAMATION
//     Thread* Self = (Thread*) _Self;
//     void* ptr = tmalloc_reserve(size);
//     if (ptr) {
//         tmalloc_append(Self->allocPtr, ptr);
//     }

//     return ptr;
// #else
//     return malloc(size);
// #endif
    return 0;
}

/* =============================================================================
 * TxFree
 *
 * CCM: simple transactional memory de-allocation
 * =============================================================================
 */
void TxFree(void* _Self, void* ptr) {
// #ifdef TXNL_MEM_RECLAMATION
//     Thread* Self = (Thread*) _Self;
//     tmalloc_append(Self->freePtr, ptr);
// #else
// //    free(ptr);
// #endif
}
